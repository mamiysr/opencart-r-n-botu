<?php 


function ggAktarim($urun){
	global $db;

	$bresim = 'data/gittigidiyor/'.$urun->productId.'.jpg'; // Resimlerin Kaydedileceği Klasör  + Resim Adı
	$x = fopen($urun->product->photos->photo->url,'b');
	if($x === false){ //birden fazlaysa tek resim almak için
		file_put_contents('../image/'.$bresim, file_get_contents($urun->product->photos->photo['0']->url));
	}else{
		file_put_contents('../image/'.$bresim, file_get_contents($urun->product->photos->photo->url));
	}

	$data_pr = array(
		'model' => 'GG'.$urun->productId,// Ürün Kodu örn: GG1234
		'quantity' => '100', // Adet
		'stock_status_id' => '7', 
		'image' => $bresim,
		'manufacturer_id' => '0',
		'shipping' => '1',
		'price' => $urun->product->buyNowPrice,
		'points' => '0',
		'tax_class_id' => '0',
		'date_available' => $db->now(),
		'weight' => '0',
		'weight_class_id' => '2',
		'length' => '0',
		'width' => '0',
		'height' => '0',
		'length_class_id' => '1',
		'subtract' => '1',
		'minimum' => '1',
		'sort_order' => '1',
		'status' => '1',
		'date_added' => $db->now(),
		'date_modified' => $db->now()
		);

	$data_get_id = $db->insert('product', $data_pr);

	$data_desc = array(
		'product_id' => $data_get_id,
		'language_id' => '2',
		'name' => $urun->product->title,
		'description' => $urun->product->description,
		'meta_description' => $urun->product->title,
		'meta_keyword' => $urun->product->title,
		'tag' => $urun->product->title,
		);
	$data_desc_q = $db->insert('product_description',$data_desc);

	$data_cat = array(
		'product_id' => $data_get_id,
		'category_id' => '88'); // Hangi Kategoriye eklenecek ? 

	$data_cat_q = $db->insert('product_to_category',$data_cat);

	$data_store = array(
		'product_id' => $data_get_id,
		'store_id' => '0'); // Hangi Magazaya eklenecek ? 

	$data_store_q = $db->insert('product_to_store',$data_store);

	return true;
}