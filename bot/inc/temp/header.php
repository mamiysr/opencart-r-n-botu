<!DOCTYPE html>
<html>
<head>
	<title>YSR Opencart İçe Aktarma Botu</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="./inc/temp/assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="./inc/temp/assets/css/ysr.css" />

	<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<header>
					<nav class="navbar navbar-inverse">
						<div class="container-fluid">

							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navmenu">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								<a class="navbar-brand" target="_blank" href="http://mamiysr.com">YSR</a>
							</div>

							<div class="collapse navbar-collapse" id="navmenu">
								<ul class="nav navbar-nav">
									<li <?php echo (!isset($islem)) ? 'class="active"' : ''; ?>><a href="./">Anasayfa</a></li>
									
									<li class="<?php echo isset($islem) ? 'active' : ''; ?> dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Ürün Aktarma Sihirbazı<span class="caret"></span></a>
										<ul class="dropdown-menu" role="menu">
											<li <?php echo isset($islem) && strpos($islem,'gg') !== false ? 'class="active"' : ''; ?>>
												<a href="./?i=ggSihirbaz">GittiGidiyor</a>
											</li>
										</ul>
									</li>
								</ul>
								<!--
								<ul class="nav navbar-nav navbar-right">
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">GittiGidiyor.Com <span class="caret"></span></a>
										<ul class="dropdown-menu" role="menu">
											<li><a href="#">Dükkan Bilgisi</a></li>
											<li><a href="#">Kategoriler</a></li>
											<li><a href="#">Ürünler</a></li>
											<li><a href="#">Sattıklarım</a></li>
											<li><a href="#">Satılmayanlar</a></li>
											<li class="divider"></li>
											<li><a href="#">Gittigidiyor » Opencart</a></li>
											<li class="divider"></li>
											<li><a href="#">Gittigidiyor Ayarları</a></li>
										</ul>
									</li>
									<li><a href="#">Ayarlar</a></li>
								</ul>
								-->
							</div>
							
						</div>
					</nav>
				</header>	
			</div>
		</div>