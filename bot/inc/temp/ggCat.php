<?php include('header.php'); ?>
		<div class="row">
			<div class="col-lg-12">

				<div class="progress progress-striped active">
					<div class="progress-bar progress-bar-success" style="width: 35%">Kategori Seç</div>
					<div class="progress-bar progress-bar-warning" style="width: 35%">Aktarılıyor</div>
					<div class="progress-bar progress-bar-danger" style="width: <?php echo isset($sonuc)? '30': '0';?>%"><?php echo isset($sonuc) ? 'Aktarıldı': 'Bekleniyor'; ?></div>
				</div>

				<?php if(isset($sonuc)): ?>
					<div class="alert alert-dismissible alert-success">
					  <button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>Başarılı!</strong> Ürünler Aktarıldı.
					</div>
				<?php else: ?>
					<div class="alert alert-dismissible alert-success">
					  <button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>Sabırlı Olun!</strong> Aktarılıyor,  <a href="#" class="alert-link"><?php echo $products->productCount;?></a> Üründen, <a href="#" class="alert-link"><?php echo $sira; ?></a> Aktarılıyor...
					</div>
				<?php endif; ?>

			</div>

		</div>
<?php include('footer.php'); ?>
