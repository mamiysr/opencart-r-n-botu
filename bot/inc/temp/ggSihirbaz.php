<?php include('header.php'); ?>
		<div class="row">
			<div class="col-lg-12">

				<div class="progress progress-striped active">
					<div class="progress-bar progress-bar-success" style="width: 35%">Kategori Seç</div>
					<div class="progress-bar progress-bar-warning" style="width: 0%"></div>
					<div class="progress-bar progress-bar-danger" style="width: 0%"></div>
				</div>

				<div class="well well-sm">
				<form method="get" action="?">
				<input type="hidden" name="i" value="ggCat" />
				<input type="hidden" name="s" value="0">
					<table class="table table-striped table-hover ">
						<thead>
							<tr>
								<th>##</th>
								<th># ID # </th>
								<th>Kategori Adı</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($storeBilgi->store->categories as $kats): ?>
							<?php foreach($kats as $kat): ?> 
							<tr>
								<td><input type="checkbox" name="kat_id[]" value="<?php echo $kat->id; ?>"></td>
								<td><?php echo $kat->id; ?></td>
								<td><?php echo $kat->name; ?></td>
							</tr>
							<?php endforeach; ?>
						<?php endforeach; ?>
						</tbody>
					</table> 
					<button type="submit" class="btn btn-block btn-success">Devam »</button>
				</form>
				</div>
				
			</div>

		</div>
		
<?php include('footer.php'); ?>
