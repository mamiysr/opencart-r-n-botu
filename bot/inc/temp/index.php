<?php include('header.php'); ?>
		<div class="row">

			<div class="col-lg-12">
				<div class="well">
					<table class="table table-striped table-hover ">
						<thead>
							<tr>
								<th># ID # </th>
								<th>Kategori Adı</th>
								<th>İşlemler</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Column content</td>
								<td>Column content</td>
							</tr>
						</tbody>
					</table> 
					<div class="text-center">
						<ul class="pagination pagination-sm">
							<li class="disabled"><a href="#">«</a></li>
							<li class="active"><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#">»</a></li>
						</ul>
					</div>
				</div>
			</div>

		</div>
<?php include('footer.php'); ?>
